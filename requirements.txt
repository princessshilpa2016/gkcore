pyramid == 2.0
psycopg2 == 2.9.3  # previously 2.8.6
requests == 2.25.0
sqlalchemy == 1.3.20
monthdelta == 0.9.1
pyjwt == 1.7.1
pycryptodome == 3.9.9
natsort == 7.1.0
gunicorn==20.1.0
pillow == 8.0.1
wsgicors == 0.7.0
openpyxl == 2.5.0
black == 22.1.0
jsonschema == 4.0.1
# supervisor == 4.2.1
